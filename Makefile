# .. title:: Makefile for myr Docker container
# .. date:: July 2018
# .. author:: Patrick Marchand
#
#.SILENT
.PHONY: all essai buildR build
NO_COLOR=\033[0m
OK_COLOR=\033[32;01m    # green
ERROR_COLOR=\033[31;01m # red
WARN_COLOR=\033[33;01m  # brown/orange

# definition de variables secretes, fichier à placer dans gitignore
#include make_env

# image identification
# user gitlab
NS ?= marchandp

VERSION ?= latest
IMAGE_NAME ?= myr
CONTAINER_NAME ?= myrcontainer
CONTAINER_INSTANCE ?= default
# Set dir of Makefile to a variable to use later
MAKEPATH := $(abspath $(lastword $(MAKEFILE_LIST)))
PWD := $(dir $(MAKEPATH)) # print working directory
# racourcis sur commandes docker

all: 
test: Dockerfile
	echo "fichier Dockerfile modifié !";\
	touch test
build: Dockerfile
	docker build -t $(NS)/$(IMAGE_NAME):$(VERSION) -f Dockerfile .
	touch build
push: build
	docker push $(NS)/$(IMAGE_NAME):$(VERSION)
pull: build
	docker pull $(NS)/$(IMAGE_NAME):$(VERSION)
release: build
	make push -e VERSION=$(VERSION)
default: build

buildandpull: 
	git push origin master
	make build
	make pull s

dockerfile: Dockerfile
	git fetch origin master
	git add Dockerfile
	git commit -m "Dockerfile"
	make buildandpull

requirements: requirements.R
	git fetch origin master
	git add requirements.R
	git commit -m "requirements.R"
	make buildandpull
makefile: Makefile
	git fetch origin master
	git add Makefile
	git commit -m "Makefile"
	git push origin master
buildR:
	echo ""buildR à ecrire"
#
# sources:
# -------
# https://www.youtube.com/watch?v=2VV9FAQWHdw
#
# https://gist.githubusercontent.com/isaacs/62a2d1825d04437c6f08/raw/fcc177b6b07469605f42c6b7b5c79955620120e7/Makefile
# couleurs
# https://stackoverflow.com/questions/5947742/how-to-change-the-output-color-of-echo-in-linux

