FROM rocker/tidyverse
LABEL maintainer="Patrick Marchand <patrick.marchand@cetim.fr>"

WORKDIR /app
RUN ls -l && pwd
COPY requirements.R /app/requirements.R
COPY requirements2.R /app/requirements2.R
# variable utilisée dans requirements.R
ARG DOCKER_MYPASSWRD 
ENV DOCKER_MYPASSWRD = $DOCKER_MYPASSWRD
RUN echo $DOCKER_MYPASSWRD
#pas OK
#RUN Rscript --vanilla -e 'devtools::install_git(paste(c("https://marchandp:","$DOCKER_MYPASSWRD","@gitlab.com/marchandp/contourPM.git"),collapse=""))' 
#RUN Rscript --vanilla -e 'devtools::install_git(paste(c("https://marchandp:",$DOCKER_MYPASSWRD,"@gitlab.com/marchandp/contourPM.git"),collapse=""))' 
#RUN Rscript --vanilla -e devtools::install_git(paste(c("https://marchandp:",$DOCKER_MYPASSWRD,"@gitlab.com/marchandp/contourPM.git"),collapse="")) 
#RUN Rscript --vanilla -e devtools::install_git(paste(c("https://marchandp:","$DOCKER_MYPASSWRD","@gitlab.com/marchandp/contourPM.git"),collapse="")) 
#RUN Rscript --vanilla -e 'devtools::install_git("https://gitlab.com/marchandp/contourPM.git")' 

# Elements nécessaires pour le paquet rgl https://stackoverflow.com/questions/31820865/error-in-installing-rgl-package
RUN export DEBIAN_FRONTEND=noninteractive && \
   apt-get update && apt-get install -y \
   xorg \
   libx11-dev  \
   libglu1-mesa-dev \
   libfreetype6-dev 
   # éléments pour imager https://github.com/dahtah/imager
   # libx11-dev  \
RUN  apt-get install -y \
   libfftw3-dev \ 
   libxt-dev \
   libjpeg-dev \
   liblzma-dev \
   liblz-dev \
   zlib1g-dev \
   libtiff5-dev
## R's X11 runtime dependencies
RUN apt-get install -y --no-install-recommends \
    libx11-6 \
    libxss1 \
    libxt6 \
    libxext6 \
    libsm6 \
    libice6 \
    xdg-utils \
  && rm -rf /var/lib/apt/lists/*
RUN apt-get update && apt-get install -y \
    tk \
    tcl \
    tk-dev \
    tcl-dev \
    tk8.6 \
    tcl8.6 \
#    && apt-get install  -y tk-dev \
    libudunits2-dev
RUN apt-get install -y software-properties-common \
    gnupg \
    gnupg2 \
    gnupg1
#RUN add-apt-repository "deb http://mirror.ibcp.fr/pub/CRAN/bin/linux/ubuntu xenial/" 
#RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E084DAB9
#RUN apt-get update && apt-get upgrade -y && apt-get dist-upgrade -y && apt-get install libgdal1 
#libgdal1-dev

# https://github.com/rocker-org/geospatial/blob/master/3.5.1/Dockerfile
RUN apt-get update \
  && apt-get install -y --no-install-recommends \
    libgdal-dev
RUN apt-get install -qy libgeos-dev libgeos-c1v5 gdal-bin netcdf-bin libproj-dev
#RUN apt-get build-dep -y r-cran-rgeos r-cran-rgdal
#RUN echo "Oh dang look at that $DOCKER_MYPASSWRD"
RUN Rscript /app/requirements.R
RUN Rscript --vanilla -e 'devtools::install_git("https://marchandp:bonbonschocolat@gitlab.com/marchandp/contourPM.git")' 

# https://www.r-bloggers.com/passing-arguments-to-an-r-script-from-command-lines/
RUN Rscript /app/requirements2.R
# Install TinyTeX (subset of TeXLive)
# From FAQ 5 and 6 here: https://yihui.name/tinytex/faq/
# Also install ae, parskip, and listings packages to build R vignettes

## Use tinytex for LaTeX installation
RUN wget -qO- \
    "https://github.com/yihui/tinytex/raw/master/tools/install-unx.sh" | \
    sh -s - --admin --no-path \
  && mv ~/.TinyTeX /opt/TinyTeX \
  && /opt/TinyTeX/bin/*/tlmgr path add \
  && tlmgr install metafont mfware inconsolata tex ae parskip listings \
  && tlmgr path add \
  && Rscript -e "source('https://install-github.me/yihui/tinytex'); tinytex::r_texmf()" \
  && chown -R root:staff /opt/TinyTeX \
  && chmod -R g+w /opt/TinyTeX \
  && chmod -R g+wx /opt/TinyTeX/bin \
 ## And some nice R packages for publishing-related stuff
  && install2.r --error --deps TRUE \
    bookdown rticles rmdshower DT


ADD . /app
# On expose le port 8787
EXPOSE 8787
# ENTRYPOINT ["/bin/bash", "-c"]
